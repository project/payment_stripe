Description
-----------
Payment Stripe allows you to use the [Payment](https://www.drupal.org/project/payment) and
[Stripe](https://www.drupal.org/project/stripe) modules to processes Stripe payments on Drupal 8.
You cannot run Payment Stripe without Payment and Stripe modules.

Installation
------------
To install this module, you will require [Composer](https://getcomposer.org) on your system.
Install that first if it isn't already installed.

On the command line, instruct composer to add the module to your project:

```cd <project directory>```

```composer require drupal/payment_stripe```

Or, add the following to your project composer.json file with a text editor:

```"drupal/payment_stripe": "^1.0"```

Then run:
```composer update```

Installing the module without Composer is not recommended and is unsupported; as of Drupal 8.4 all sites should use Composer.

Configuration
-------------------
After Payment Stripe is installed you must do some configuration.
- In (admin/config/services/payment/method/configuration-add) you must add payment method of Stripe Payment,
  in the Execution tab set 'Payment execution status' in Completed to set payments
  to after being executed by this payment method.
- In (admin/config/services/payment/type/payment_stripe/form-display) and (admin/config/services/payment/type/  payment_stripe/display) enable 'Newsletter' field.
- In (/admin/config/stripe) set the 'Publishable', 'Secret', 'Webhook secret' values to process Stripe payments.

Enabling the Module
-------------------
Enable the module by choosing it from /admin/modules or use Drush:

```cd <project directory/HTML directory>```

```drush en -y payment_stripe```

Allow Drupal to enable all the dependent modules.

Dependencies
------------
Payment Stripe depends on [Payment](https://www.drupal.org/project/payment) and
[Stripe](https://www.drupal.org/project/stripe). See the payment_stripe_info.yml file for the complete list.

Documentation
-------------
The Payment Stripe module has its own documentation at:
https://performantlabs.com/campaign-kit/payment-stripe-module-documentation
