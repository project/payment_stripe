<?php

namespace Drupal\payment_stripe\Plugin\Payment\Method;

use Drupal\payment\Plugin\Payment\Method\PaymentMethodConfigurationOperationsProvider;

/**
 * Provides payment_stripe operations based on config entities.
 */
class StripePaymentOperationsProvider extends PaymentMethodConfigurationOperationsProvider {

  /**
   * {@inheritdoc}
   */
  protected function getPaymentMethodConfiguration($plugin_id) {
    $entity_id = substr($plugin_id, 15);
    return $this->paymentMethodConfigurationStorage->load($entity_id);
  }

}
