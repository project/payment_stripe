<?php

namespace Drupal\payment_stripe\EventSubscriber;

use Drupal\payment_stripe\Event\StripeEvents;
use Drupal\payment_stripe\Event\StripeWebhookChargeFailed;
use Drupal\payment_stripe\Event\StripeWebhookChargeSucceeded;
use Drupal\payment_stripe\Event\StripeWebhookEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ExampleEventSubscriber.
 *
 * @package Drupal\payment_stripe
 */
class StripeEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[StripeEvents::WEBHOOK][] = ['onStripeWebhook'];
    $events[StripeEvents::WEBHOOK_CHARGE_SUCCEEDED][] = ['onStripeChargeSucceed'];
    $events[StripeEvents::WEBHOOK_CHARGE_FAILED][] = ['onStripeChargeFailed'];
    return $events;
  }

  /**
   * Event when Charge succeeded.
   *
   * @param \Drupal\payment_stripe\Event\StripeWebhookChargeSucceeded $event
   *   The Stripe Webhook Charge Succeeded.
   */
  public function onStripeChargeSucceed(StripeWebhookChargeSucceeded $event) {
    $eventStripe = $event->getEvent();
    $config = \Drupal::config('payment_stripe.settings');
    // Get all values in array.
    $chargeArray = $eventStripe->jsonSerialize();
    if ($config->get('debug_mode')) {
      \Drupal::logger('payment_stripe')->notice('onStripeChargeSucceed: Data Charge Succeeded alvaro[description]: @id.', ['@id' => $eventStripe->id]);
      \Drupal::logger('payment_stripe')->notice('onStripeChargeSucceed: $chargeArray: @id.', ['@id' => $chargeArray['id']]);
      \Drupal::logger('payment_stripe')->notice('onStripeChargeSucceed: $dataType: @status.', ['@status' => $chargeArray['status']]);
    }
  }

  /**
   * Event when Charge Failed.
   *
   * @param \Drupal\payment_stripe\Event\StripeWebhookChargeFailed $event
   *   The Stripe Webhook Charge Failed.
   */
  public function onStripeChargeFailed(StripeWebhookChargeFailed $event) {
    $config = \Drupal::config('payment_stripe.settings');
    if ($config->get('debug_mode')) {
      \Drupal::logger('payment_stripe')->notice('onStripeChargeFailed received from Stripe dashboard: @stripeId.', ['@stripeId' => $event->getStripeId()]);
    }
  }

  /**
   * Event when Stripe Webhook fires.
   *
   * @param \Drupal\payment_stripe\Event\StripeWebhookEvent $event
   *   The Stripe Webhook Event.
   */
  public function onStripeWebhook(StripeWebhookEvent $event) {
    $config = \Drupal::config('payment_stripe.settings');
    if ($config->get('debug_mode')) {
      \Drupal::logger('payment_stripe')->notice('onStripeWebhook from WEB: @type.', ['@type' => $event->getEvent()->type]);
    }
  }

}
