<?php

namespace Drupal\payment_stripe\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\payment_stripe\Event\StripeEvents;
use Drupal\payment_stripe\Event\StripeWebhookChargeFailed;
use Drupal\payment_stripe\Event\StripeWebhookChargeSucceeded;
use Drupal\payment_stripe\Event\StripeWebhookEvent;
use Stripe\Error\SignatureVerification;
use Stripe\Event;
use Stripe\Stripe;
use Stripe\Webhook;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routines for book routes.
 */
class StripeWebhookController extends ControllerBase {

  /**
   * The Event Dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Event Dispatcher.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The Event dispatcher.
   */
  public function __construct(EventDispatcherInterface $eventDispatcher) {
    $this->event_dispatcher = $eventDispatcher;
  }

  /**
   * This Controller creates a connection with Stripe Payment.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Container Object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher')
    );
  }

  /**
   * Handle webhook.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Request Object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The HTTP Response.
   */
  public function handle(Request $request) {
    $config = \Drupal::config('payment_stripe.settings');
    /** @var \Stripe\Stripe $event */
    Stripe::setApiKey($config->get('apikey.' . $config->get('environment') . '.secret'));

    $environment = $config->get('environment');
    $payload = @file_get_contents("php://input");
    $signature = $request->server->get('HTTP_STRIPE_SIGNATURE');
    $secret = $config->get("apikey.$environment.webhook");
    /** @var \Stripe\Event $event */
    try {
      if (!empty($secret)) {
        $event = Webhook::constructEvent($payload, $signature, $secret);
      }
      else {
        $data = json_decode($payload, TRUE);
        $jsonError = json_last_error();
        if ($data === NULL && $jsonError !== JSON_ERROR_NONE) {
          $msg = 'Invalid payload: $payload ' . '(json_last_error() was $jsonError)';
          throw new \UnexpectedValueException($msg);
        }

        if ($environment == 'live') {
          $event = Event::retrieve($data['id']);
        }
        else {
          $event = Event::constructFrom($data, NULL);
        }
      }
    }
    catch (\UnexpectedValueException $e) {
      return new Response('Invalid payload', Response::HTTP_BAD_REQUEST);
    }
    catch (SignatureVerification $e) {
      return new Response('Invalid signature', Response::HTTP_BAD_REQUEST);
    }

    // Dispatch the webhook event.
    $this->event_dispatcher
      ->dispatch(StripeEvents::WEBHOOK, new StripeWebhookEvent($event));

    // Query DB and modify Payment if it is necessary according webhook type.
    switch ($event->type) {
      case 'charge.succeeded':
        if ($config->get('debug_mode')) {
          \Drupal::logger('payment_stripe')->notice('Webhook for @type.', ['@type' => $event->type]);
        }
        break;

      case 'charge.failed':
        if ($config->get('debug_mode')) {
          \Drupal::logger('payment_stripe')->notice('Webhook for @type.', ['@type' => $event->type]);
        }
        break;
    }

    return new Response('OK', Response::HTTP_OK);
  }

  /**
   * Charge Succeeded webhook.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Request to work on.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   HTTP response.
   */
  public function chargeSucceeded(Request $request) {
    $config = \Drupal::config('payment_stripe.settings');
    /** @var \Stripe\Stripe $event */
    Stripe::setApiKey($config->get('apikey.' . $config->get('environment') . '.secret'));

    $environment = $config->get('environment');
    $payload = @file_get_contents('php://input');
    $signature = $request->server->get('HTTP_STRIPE_SIGNATURE');
    $secret = $config->get("apikey.$environment.webhook");
    /** @var \Stripe\Event $event */
    try {
      if (!empty($secret)) {
        $event = Webhook::constructEvent($payload, $signature, $secret);
      }
      else {
        $data = json_decode($payload, TRUE);
        $jsonError = json_last_error();
        if ($data === NULL && $jsonError !== JSON_ERROR_NONE) {
          $msg = "Invalid payload: $payload "
            . "(json_last_error() was $jsonError)";
          throw new \UnexpectedValueException($msg);
        }

        if ($environment == 'live') {
          $event = Event::retrieve($data['id']);
        }
        else {
          /** @var \Stripe\Event $event */
          $event = Event::constructFrom($data, NULL);
        }
      }
    }
    catch (\UnexpectedValueException $e) {
      return new Response('Invalid payload', Response::HTTP_BAD_REQUEST);
    }
    catch (SignatureVerification $e) {
      return new Response('Invalid signature', Response::HTTP_BAD_REQUEST);
    }

    // Dispatch the webhook charge succeeded event.
    $this->event_dispatcher
      ->dispatch(StripeEvents::WEBHOOK_CHARGE_SUCCEEDED, new StripeWebhookChargeSucceeded($event));

    if ($config->get('debug_mode')) {
      \Drupal::logger('payment_stripe')->notice('Charge succeeded @type.', ['@type' => $event->type]);
    }

    return new Response('OK', Response::HTTP_OK);
  }

  /**
   * Charge Failed webhook.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Request to be work on.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   HTTP response.
   */
  public function chargeFailed(Request $request) {
    $config = \Drupal::config('payment_stripe.settings');
    /** @var \Stripe\Stripe $event */
    Stripe::setApiKey($config->get('apikey.' . $config->get('environment') . '.secret'));

    $environment = $config->get('environment');
    $payload = @file_get_contents('php://input');
    $signature = $request->server->get('HTTP_STRIPE_SIGNATURE');
    $secret = $config->get("apikey.$environment.webhook");
    /** @var \Stripe\Event $event */
    try {
      if (!empty($secret)) {
        $event = Webhook::constructEvent($payload, $signature, $secret);
      }
      else {
        $data = json_decode($payload, TRUE);
        $jsonError = json_last_error();
        if ($data === NULL && $jsonError !== JSON_ERROR_NONE) {
          $msg = "Invalid payload: $payload "
            . "(json_last_error() was $jsonError)";
          throw new \UnexpectedValueException($msg);
        }

        if ($environment == 'live') {
          $event = Event::retrieve($data['id']);
        }
        else {
          $event = Event::constructFrom($data, NULL);
        }
      }
    }
    catch (\UnexpectedValueException $e) {
      return new Response('Invalid payload', Response::HTTP_BAD_REQUEST);
    }
    catch (SignatureVerification $e) {
      return new Response('Invalid signature', Response::HTTP_BAD_REQUEST);
    }

    // Dispatch the webhook charge succeeded event.
    $this->event_dispatcher
      ->dispatch(StripeEvents::WEBHOOK_CHARGE_FAILED, new StripeWebhookChargeFailed($event));

    if ($config->get('debug_mode')) {
      \Drupal::logger('payment_stripe')->notice('ChargeFailed @type.', ['@type' => $event->type]);
    }

    return new Response('OK', Response::HTTP_OK);
  }

}
