<?php

namespace Drupal\payment_stripe\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\payment\Plugin\Payment\Type\PaymentTypeManager;

/**
 * Provide extra functions for Payment Stripe.
 *
 * @package Drupal\payment_stripe\Service
 */
class Service {
  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The service logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerChannelFactory;

  /**
   * The Payment Type Manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Type\PaymentTypeManager
   */
  protected $paymentTypeManager;

  /**
   * Service constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerChannelFactory
   *   The service logger factory.
   * @param \Drupal\payment\Plugin\Payment\Type\PaymentTypeManager $paymentTypeManager
   *   The payment type manager.
   */
  public function __construct(ConfigFactory $configFactory, LoggerChannelFactory $loggerChannelFactory, PaymentTypeManager $paymentTypeManager) {
    $this->configFactory = $configFactory;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->paymentTypeManager = $paymentTypeManager;
  }

  /**
   * Get Config Factory.
   *
   * @param $nameConfig
   *
   * @return \Drupal\Core\Config\Config|ImmutableConfig
   */
  public function getConfigFactory($nameConfig) {
    return $this->configFactory->get($nameConfig);
  }

  /**
   * Helper function to check Payment API Keys.
   *
   * @param string $config
   *   The config factory service.
   *
   * @return bool
   *   Boolean status, TRUE or FALSE.
   */
  public function checkTestPaymentApiKey($config) {
    $status = FALSE;
    $provider = '';

    // Get the provider.
    foreach ($this->paymentTypeManager->getDefinitions() as $pluginId => $definition) {
      $provider = $definition['provider'];
    }

    // We can add more payments here.
    switch ($provider) {
      case 'payment_stripe':
        // Do not forget to change this to live when it is in production.
        if ($config->get('environment') == 'test' && $config->get('apikey.test.secret')) {
          $status = TRUE;
        }
        break;
    }

    return $status;
  }

}
