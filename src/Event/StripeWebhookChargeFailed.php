<?php

namespace Drupal\payment_stripe\Event;

use Stripe\Charge;
// Use Symfony\Component\EventDispatcher\Event;.
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class StripeWebhookChargeFailed.
 */
class StripeWebhookChargeFailed extends Event {

  /**
   * The Stripe Event.
   *
   * @var \Stripe\Event
   */
  protected $event;

  /**
   * StripeWebhookChargeFailed constructor.
   *
   * @param \Stripe\Charge $event
   *   Stripe Charge.
   */
  public function __construct(Charge $event) {
    $this->event = $event;
  }

  /**
   * Get a Stripe Event.
   *
   * @return \Stripe\Event
   *   Return a Stripe Event.
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * Get Stripe ID.
   *
   * @return string
   *   Return a Stripe ID.
   */
  public function getStripeId() {
    return $this->event->id;
  }

}
