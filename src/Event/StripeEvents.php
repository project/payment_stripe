<?php

namespace Drupal\payment_stripe\Event;

/**
 * Defines events for stripe webhooks.
 */
final class StripeEvents {

  /**
   * The name of the event fired when a webhook is received.
   *
   * @Event
   */
  const WEBHOOK = 'stripe.webhook';

  /**
   * The name of the event that is fired before sending a Credit card inside form.
   */
  const PAYMENT_PRE_AUTHORIZE = 'stripe.payment_pre_authorize';

  /**
   * The name of the event fired when a webhook charge succeeded is received.
   *
   * @Event
   */
  const WEBHOOK_CHARGE_SUCCEEDED = 'stripe.webhook_charge_succeeded';

  /**
   * The name of the event fired when a webhook charge failed is received.
   *
   * @Event
   */
  const WEBHOOK_CHARGE_FAILED = 'stripe.webhook_charge_failed';

}
