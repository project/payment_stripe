<?php

namespace Drupal\payment_stripe\Event;

use Stripe\Event as StripeEvent;
// Use Symfony\Component\EventDispatcher\Event;.
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Wraps a stripe event for webhook.
 */
class StripeWebhookEvent extends Event {

  /**
   * Stripe API event object.
   *
   * @var \Stripe\Event
   */
  protected $event;

  /**
   * Construct a Stripe Webhook Event object.
   *
   * @param \Stripe\Event $event
   *   Stripe API event object object.
   */
  public function __construct(StripeEvent $event) {
    $this->event = $event;
  }

  /**
   * Get Stripe API event object.
   *
   * @return \Stripe\Event
   *   The Stripe API event object
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * Get Stripe ID.
   *
   * @return string
   *   Return Stripe ID.
   */
  public function getStripeId() {
    return $this->event->id;
  }

}
